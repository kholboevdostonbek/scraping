package main

import (
	"net"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	// ph "github.com/defval/parsehub"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	"gitlab.com/scraping/config"
	pb "gitlab.com/scraping/genproto/product_service"
	"gitlab.com/scraping/pkg/logger"
	"gitlab.com/scraping/service"
)

func main() {

	if info, err := os.Stat(".env"); !os.IsNotExist(err) {
		if !info.IsDir() {
			godotenv.Load(".env")
		}
	}
	cfg := config.Get()

	log := logger.New(cfg.LogLevel, "product_service")
	defer logger.Cleanup(log)

	s := grpc.NewServer()

	productService := service.NewProductService(log)
	pb.RegisterProductServiceServer(s, productService)

	reflection.Register(s)
	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	log.Fatal("API server has finished")

}
