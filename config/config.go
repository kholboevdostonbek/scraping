package config

import (
	"os"
	"sync"

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	"github.com/spf13/cast"
)

const (
	// NewStatus enum
	NewStatus = "new"
	// SentStatus enum
	SentStatus = "sent"
	// SubscribeType enum
	SubscribeType = "subscribe"
	// PostType enum
	PostType = "post"
	// UnsubscribeType enum
	UnsubscribeType = "unsubscribe"
	// LikeType enum
	LikeType = "like"
	// JoinType enum
	JoinType = "join"
	// CommentType enum
	CommentType = "comment"
	// DislikeType enum
	DislikeType = "dislike"
	// GiftType enum
	GiftType = "gift"
	// Replied enum
	Replied = "replied"
	// TipType enum
	TipType = "tip"
	// TagType enum
	TagType = "tag"
)

//Config ...
type Config struct {
	Environment             string
	PostgresHost            string
	PostgresPort            int
	PostgresDatabase        string
	PostgresUser            string
	PostgresPassword        string
	LogLevel                string
	RPCPort                 string
	NotificationServiceHost string
	NotificationServicePort int
	FirebaseWebKey          string
	DomainURIPrefix         string
	AndroidPackageName      string
	IosBundleID             string
	SentryDNS               string
	RapidKey                string
}

func load() *Config {
	return &Config{
		Environment:             cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop")),
		PostgresHost:            cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost")),
		PostgresPort:            cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432)),
		PostgresDatabase:        cast.ToString(getOrReturnDefault("POSTGRES_DB", "")),
		PostgresUser:            cast.ToString(getOrReturnDefault("POSTGRES_USER", "")),
		PostgresPassword:        cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "")),
		LogLevel:                cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug")),
		RPCPort:                 cast.ToString(getOrReturnDefault("RPC_PORT", ":9000")),
		NotificationServiceHost: cast.ToString(getOrReturnDefault("NOTIFICATION_SERVICE_HOST", "127.0.0.1")),
		NotificationServicePort: cast.ToInt(getOrReturnDefault("NOTIFICATION_SERVICE_PORT", 9005)),
		FirebaseWebKey:          cast.ToString(getOrReturnDefault("FIREBASE_WEB_KEY", "")),
		AndroidPackageName:      cast.ToString(getOrReturnDefault("ANDROID_PACKAGE_NAME", "")),
		IosBundleID:             cast.ToString(getOrReturnDefault("IOS_BUNDLE_ID", "")),
		DomainURIPrefix:         cast.ToString(getOrReturnDefault("DOMAIN_URI_PREFIX", "")),
		SentryDNS:               cast.ToString(getOrReturnDefault("SENTRY_DNS", "")),
		RapidKey:                cast.ToString(getOrReturnDefault("RAPID_KEY", "")),
	}
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}

var (
	instance *Config
	once     sync.Once
)

//Get ...
func Get() *Config {
	once.Do(func() {
		instance = load()
	})

	return instance
}
