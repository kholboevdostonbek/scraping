package utils

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/scraping/config"
)

// GetPostImageOriginalURL ...
func GetPostImageOriginalURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage.s3.amazonaws.com/%s", mediaID)
}

// GetProductImageCompressedURL ...
func GetProductImageCompressedURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/images/products/compressed/%s.jpg", mediaID)
}

// GetProductImage240pURL ...
func GetProductImage240pURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/images/products/240x240/%s.jpg", mediaID)
}

// GetReviewImageCompressedURL ...
func GetReviewImageCompressedURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/images/reviews/compressed/%s.jpg", mediaID)
}

// GetReviewImage250pURL ...
func GetReviewImage250pURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/images/reviews/250p/%s.jpg", mediaID)
}

// GetReviewVideoURL ...
func GetReviewVideoURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/videos/reviews/compressed/%s.mp4", mediaID)
}

// GetReviewVideoThumbnailURL ...
func GetReviewVideoThumbnailURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/videos/reviews/thumbnails/original/%s.jpg", mediaID)
}

// GetPostImageBlurURL ...
func GetPostImageBlurURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/images/blur/%s.jpg", mediaID)
}

// GetAvatar300x300URL ...
func GetAvatar300x300URL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/avatars/300x300/%s.jpg", mediaID)
}

// GetAvatar500x500URL ...
func GetAvatar500x500URL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/avatars/500x500/%s.jpg", mediaID)
}

// GetPlaylist500x500URL ...
func GetPlaylist500x500URL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/playlist/%s.jpg", mediaID)
}

// GetPostVideo720pURL ...
func GetPostVideo720pURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/videos/720p/%s.mp4", mediaID)
}

// GetPostVideo720pThumbnailURL ...
func GetPostVideo720pThumbnailURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/videos/thumbnails/%s.jpg", mediaID)
}

// GetPostVideoBlurURL ...
func GetPostVideoBlurURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/videos/blur/%s.jpg", mediaID)
}

// GetStoryVideo720pURL ...
func GetStoryVideo720pURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/stories/videos/%s.mp4", mediaID)
}

// GetStoryThumbnailURL ...
func GetStoryThumbnailURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/stories/thumbnails/%s.jpg", mediaID)
}

// GetStoryImageOriginalURL ...
func GetStoryImageOriginalURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage.s3.amazonaws.com/%s", mediaID)
}

// GetStoryImageURL ...
func GetStoryImageURL(mediaID string) string {
	return fmt.Sprintf("https://shelfish-storage-resized.s3.us-east-1.amazonaws.com/stories/images/%s.jpg", mediaID)
}

// GenDynamicLinkPost ...
func GenDynamicLinkPost(postID, description, imageURL string) string {
	var linkForFirebase string
	var result map[string]interface{}
	cfg := config.Get()

	if cfg.Environment == "develop" {
		linkForFirebase = "https://shelfish.jafton.com/posts/" + postID + "/"
	} else if cfg.Environment == "staging" {
		linkForFirebase = "https://shelfish.jafton.com/posts/" + postID + "/"
	} else {
		linkForFirebase = "https://shelfish.jafton.com/posts/" + postID + "/"
	}
	url := "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + cfg.FirebaseWebKey
	var jsonStr = []byte(`{
		"dynamicLinkInfo": {
		  "domainUriPrefix": "` + cfg.DomainURIPrefix + `",
		  "link": "` + linkForFirebase + `",
		  "androidInfo": {
			"androidPackageName": "` + cfg.AndroidPackageName + `"
		  },
		  "iosInfo": {
			"iosBundleId": "` + cfg.IosBundleID + `"
		  },
		  "socialMetaTagInfo": {
    		 "socialTitle": "check this shelfish post",
    		 "socialDescription": "` + description + `",
  			 "socialImageLink": "` + imageURL + `"
 		  },
		}
	  }`)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error in requesting firebase,", err)
		panic(err)
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		fmt.Println("Error in decoding response,", err)
		panic(err)
	}
	link := fmt.Sprintf("%s", result["shortLink"])

	return link
}

// StringToNullString ...
func StringToNullString(s string) (ns sql.NullString) {
	if s != "" {
		ns.String = s
		ns.Valid = true
	}
	return ns
}
