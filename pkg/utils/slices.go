package utils

import (
	"fmt"
	"strconv"
	"strings"
)

// Int64SliceToStringSlice ...
func Int64SliceToStringSlice(a []int64) (str []string) {
	for _, item := range a {
		str = append(str, strconv.Itoa(int(item)))
	}

	return str
}

// StringSliceToString ...
func StringSliceToString(a []string, delimiter string) string {
	return strings.Join(a, delimiter)
}

// UUIDsToString ...
func UUIDsToString(a []string) (str string) {
	for id, item := range a {
		if id == len(a)-1 {
			str += fmt.Sprintf("'%s'", item)
			return str
		}
		str += fmt.Sprintf("'%s',", item)
	}
	return str
}

// UUIDSToStringWithoutQuotes ...
func UUIDSToStringWithoutQuotes(a []string) (str string) {
	for id, item := range a {
		if id == len(a)-1 {
			str += item
			return str
		}
		str += fmt.Sprintf("%s,", item)
	}
	return str
}

// InEnums ...
func InEnums(str string, enums []string) bool {
	for _, enum := range enums {
		if enum == str {
			return true
		}
	}
	return false
}

// ReplaceSQL replaces the instance occurrence of any string pattern with an increasing $n based sequence
func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)

	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}

	return old
}
