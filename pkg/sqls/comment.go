package sqls

const (
	// InsertComment ...
	InsertComment = `INSERT INTO
		comments (
			id,
			object_type,
			object_id,
			post_id,
			user_id,
			content
        )
		VALUES ($1, $2, $3, $4, $5, $6)`

	// UpdateComment ...
	UpdateComment = `
	UPDATE comments
	SET updated_at=current_timestamp, content=$1
	WHERE id=$2
	  AND user_id=$3`

	// DeleteComment ...
	DeleteComment = `UPDATE comments
	SET deleted_at=NOW()
	WHERE id=$1
	  AND object_id=$2
	  AND user_id=$3 `

	// DeleteUserComment ...
	DeleteUserComment = `
	UPDATE comments
	SET deleted_at=current_timestamp
	WHERE user_id=$1`

	// CommentCount ...
	CommentCount = `SELECT COUNT(*) FROM comments WHERE deleted_at IS NULL AND (object_id in
	(SELECT id FROM comments where object_type = $1 and object_id = $2) or object_id = $2)`

	// ListComment ...
	ListComment = `SELECT id, created_at, object_type, object_id, user_id, content FROM comments WHERE object_type=$1 AND object_id=$2 AND
	deleted_at IS NULL ORDER BY created_at DESC LIMIT $3 OFFSET $4`

	// DetailComment ...
	DetailComment = `SELECT id, created_at, object_type, object_id, user_id, content FROM comments WHERE id=$1 AND deleted_at IS NULL `
)
