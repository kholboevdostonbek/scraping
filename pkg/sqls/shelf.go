package sqls

const (
	// InsertShelf ...
	InsertShelf = `INSERT INTO
		shelves (id, name, avatar_photo, owner_id, created_at)
		VALUES ($1, $2, $3, $4, current_timestamp)
	`

	// DeleteShelf ...
	DeleteShelf = `DELETE FROM shelves WHERE object_type=$1 AND object_id=$2 AND user_id=$3`

	// InsertShelfeProduct ...
	InsertShelfeProduct = `INSERT INTO
		shelf_products (shelf_id, product_id, created_at)
		VALUES ($1, $2, current_timestamp)
	`

	// CountShelves ...
	CountShelves = `
		SELECT count(*) from shelves WHERE deleted_at IS NULL AND owner_id = $1
	`

	// ListShelves ...
	ListShelves = `
	SELECT   
		id,
		name,
		avatar_photo,
		created_at
    FROM shelves
	WHERE owner_id=$1 AND deleted_at IS NULL
    LIMIT $2 
    OFFSET $3`
)
