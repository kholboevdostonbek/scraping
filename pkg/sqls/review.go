package sqls

const (
	// InsertReview ...
	InsertReview = `INSERT INTO
		reviews (
			id,
			message,
			created_at,
			stars,
			product_id,
			reviewer_id,
			number_of_likes,
			medias
        )
		VALUES ($1, $2, current_timestamp, $3, $4, $5, 0, $6)
	`
	
	// InsertReviewWithoutMedia ...
	InsertReviewWithoutMedia = `INSERT INTO
		reviews (
			id,
			message,
			created_at,
			stars,
			product_id,
			reviewer_id,
			number_of_likes
        )
		VALUES ($1, $2, current_timestamp, $3, $4, $5, 0)
	`

	// EditReview ...
	EditReview = `UPDATE REVIEWS
		SET 
			message = $1,
			updated_at = CURRENT_TIMESTAMP,
			stars = $2,
			medias = $3
		WHERE
			id = $4 AND reviewer_id = $5
	`
	
	// EditReviewWithoutMedia ...
	EditReviewWithoutMedia = `UPDATE REVIEWS
		SET 
			message = $1,
			updated_at = CURRENT_TIMESTAMP,
			stars = $2,
			medias=null
		WHERE
			id = $3 AND reviewer_id = $4
	`

	// ListReviews ...
	ListReviews = `
	SELECT         
		r.id,
		r.message,
		r.stars,
		r.reviewer_id,
		r.number_of_likes,
		r.created_at,
		u.username,
		u.avatar_photo, 
		r.medias,
		EXISTS (SELECT 1 FROM likes l WHERE l.user_id = $4 AND r.id=l.object_id)
    FROM reviews r JOIN users u ON r.reviewer_id=u.id
	WHERE r.product_id = $1 AND r.deleted_at IS NULL
    LIMIT $2 
    OFFSET $3`

	// ListReviewsByUserID ...
	ListReviewsByUserID = `
	SELECT         
		r.id,
		r.message,
		r.stars,
		r.number_of_likes,
		r.product_id,
		p.link,
		p.name,
		p.medias[1].value,
		p.medias[1].content_type
    FROM reviews r JOIN products p ON r.product_id=p.id
	WHERE r.reviewer_id = $1 AND r.deleted_at IS NULL
    LIMIT $2 
    OFFSET $3`

	// DeleteReview ...
	DeleteReview = `
		UPDATE reviews
		SET deleted_at = NOW()
		WHERE id = $1 AND deleted_at IS NULL AND reviewer_id = $2
	`

	// CountReviews ...
	CountReviews = `
		SELECT COUNT(*) FROM reviews WHERE deleted_at IS NULL AND product_id = $1
	`

	// CountReviewsOfUser ...
	CountReviewsOfUser = `
		SELECT COUNT(*) FROM reviews WHERE deleted_at IS NULL AND reviewer_id = $1
	`

	// CreateComment ...
	CreateComment = `
		INSERT INTO comments(id, content, created_at, review_id, user_id)
		values($1, $2, current_timestamp, $3, $4)
	`

	// CountComments ...
	CountComments = `SELECT COUNT(*) FROM comments WHERE deleted_at IS NULL AND review_id = $1`

	// ListComments ...
	ListComments = `
		SELECT
			c.id,
			c.content,
			c.created_at,
			c.review_id,
			c.number_of_likes,
			u.id,
			u.username,
			u.avatar_photo,
			EXISTS (SELECT 1 FROM likes l WHERE l.user_id = $4 AND c.id=l.object_id)
		FROM comments c JOIN users u ON c.user_id = u.id
		WHERE deleted_at IS NULL AND review_id = $1
		LIMIT $2 
    	OFFSET $3
	`

	// EditComment ...
	EditComment = `
		UPDATE comments
		SET 
			content = $1,
			updated_at = current_timestamp
		WHERE user_id = $2 AND deleted_at IS NULL;
	`

	// DeleteCommentOfReview ...
	DeleteCommentOfReview = `
		UPDATE comments
		SET deleted_at = current_timestamp
		WHERE id = $1 AND deleted_at IS NULL AND user_id = $2
	`

	// LikeCommentOfReview ...
	LikeCommentOfReview = `
		UPDATE comments 
		SET number_of_likes = number_of_likes + 1 
		WHERE id = $1
	`
)
