package sqls

const (
	// InsertLike ...
	InsertLike = `INSERT INTO
		likes (id, object_id, user_id, owner_id, created_at, object_type)
		VALUES ($1, $2, $3, $4, current_timestamp, $5)`

	// DeleteLike ...
	DeleteLike = `DELETE FROM likes WHERE object_type=$1 AND object_id=$2 AND user_id=$3`

	// DeleteUserLike ...
	DeleteUserLike = `DELETE FROM likes WHERE user_id=$1`

	// IsLiked ...
	IsLiked = `SELECT COUNT(*) FROM likes WHERE object_type=$1 AND object_id=$2 AND user_id=$3`

	// CountByObjectID ...
	CountByObjectID = `SELECT COUNT(*) FROM likes WHERE object_type=$1 AND object_id=$2`
)
