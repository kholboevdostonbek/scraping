package sqls

const (
	// InsertProduct ...
	InsertProduct = `
        INSERT INTO
		products
		(
			id,
			name,
			description,
		    link,
            comment,
            medias,
			created_at,
			creator_id,
			scraped_from
		)
		VALUES($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP, $7, $8)
    `

	// IsOwner ...
	IsOwner = `
	SELECT EXISTS(SELECT id FROM products
	WHERE id=$1 and deleted_at is null and creator_id = $2)
	`
	// IsShelfOwner ...
	IsShelfOwner = `
	SELECT EXISTS(SELECT id FROM shelves
	WHERE id=$1 and deleted_at is null and owner_id = $2)
	`

	// DeleteProduct ...
	DeleteProduct = `
		UPDATE products
		SET deleted_at = current_timestamp
		WHERE id = $1 AND deleted_at IS NULL AND creator_id = $2
	`

	// CountProducts ...
	CountProducts = `
		SELECT COUNT(*) FROM products WHERE deleted_at IS NULL
		`

	// ListProducts ...
	ListProducts = `
        SELECT
			id,
			name,
			description,
			link,
			comment,
			medias,
			created_at,
			creator_id,
			rate,
			number_of_rates
        FROM products WHERE deleted_at IS NULL
        LIMIT $1 
        OFFSET $2`

	// InsertUser ...
	InsertUser = `
        INSERT INTO
		users
		(
			id,
			username,
			avatar_photo
		)
		VALUES($1, $2, $3)
    `
	// CountProductsOfShelf ...
	CountProductsOfShelf = `
		SELECT count(*) FROM shelf_products WHERE deleted_at IS NULL and shelf_id = $1;
	`

	// ListProductsOfShelf ...
	ListProductsOfShelf = `
        SELECT
			id,
			name,
			description,
			link,
			comment,
			medias,
			created_at,
			creator_id,
			rate,
			number_of_rates
        FROM products
		WHERE deleted_at IS NULL AND id IN (SELECT product_id FROM shelf_products WHERE shelf_id = $1 AND deleted_at IS NULL)
        LIMIT $2 
        OFFSET $3
	`
	// RemoveProductFromShelf ...
	RemoveProductFromShelf = `
	UPDATE shelf_products
	SET deleted_at=current_timestamp
	WHERE shelf_id=$1 AND product_id=$2 AND deleted_at IS NULL
	`

	// EditProfile ...
	EditProfile = `
		UPDATE users
		SET username = COALESCE($2, username),
			avatar_photo = COALESCE($3, avatar_photo)
		WHERE id = $1
	`

	// IDeleteUser ...
	IDeleteUser = `
		DELETE FROM users WHERE id = $1
	`
)
