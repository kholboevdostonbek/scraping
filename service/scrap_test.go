package service_test

import (
	"context"
	"fmt"
	"os"
	"testing"
		_ "github.com/joho/godotenv/autoload" // load .env file automatically


	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scraping/config"

	pb "gitlab.com/scraping/genproto/product_service"
	"gitlab.com/scraping/pkg/logger"
	"gitlab.com/scraping/service"
)

var productServiceTest pb.ProductServiceServer

func init() {
	const path = ".env"
	if info, err := os.Stat(path); !os.IsNotExist(err) {
		if !info.IsDir() {
			godotenv.Load(path)
			if err != nil {
				fmt.Println("Err:", err)
			}
		}
	} else {
		fmt.Println("Not exists")
	}

	cfg := config.Get()
	log := logger.New(cfg.LogLevel, "product_service")
	defer logger.Cleanup(log)
	productServiceTest = service.NewProductService(log)
}

func TestFetchPost(t *testing.T) {

	t.Run("Scrap products", func(t *testing.T) {

		_, err := productServiceTest.Scrap(context.Background(), &pb.ScrapRequest{WebsiteName: "ulta"})

		assert.Equal(t, nil, err, "Error in scraping products")

	})
}
