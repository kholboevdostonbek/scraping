package grpcclient

// if you want to connect another service uncomment the code below and make some changes

// add these to imports
// "gitlab.com/scraping/config"
// "google.golang.org/grpc"
// import (
// 	userPb "gitlab.com/scraping/genproto/user_service"
// )

//IServiceManager ...
// type IServiceManager interface {
// 	UserService() userPb.UserServiceClient
// }

// type serviceManager struct {
// 	cfg                 config.Config
// 	notificationService userPb.UserServiceClient
// }

// //New ...
// func New(cfg config.Config) (IServiceManager, error) {
// 	connPost, err := grpc.Dial(
// 		fmt.Sprintf("%s:%d", cfg.userse, cfg.PostServicePort),
// 		grpc.WithInsecure())
// 	if err != nil {
// 		return nil, fmt.Errorf("post service dial host: %s port: %d",
// 			cfg.PostServiceHost, cfg.PostServicePort)
// 	}
// 	connNotification, err := grpc.Dial(
// 		fmt.Sprintf("%s:%d", cfg.NotificationServiceHost, cfg.NotificationServicePort),
// 		grpc.WithInsecure())
// 	if err != nil {
// 		return nil, fmt.Errorf("post service dial host: %s port: %d",
// 			cfg.NotificationServiceHost, cfg.NotificationServicePort)
// 	}

// 	serviceManager := &serviceManager{
// 		cfg:                 cfg,
// 		notificationService: notificationPb.NewNotificationServiceClient(connNotification),
// 		postService:         postPb.NewPostServiceClient(connPost),
// 	}

// 	return serviceManager, nil
// }

// // NotificationService ...
// func (s *serviceManager) NotificationService() notificationPb.NotificationServiceClient {
// 	return s.notificationService
// }

// // PostService ...
// func (s *serviceManager) PostService() postPb.PostServiceClient {
// 	return s.postService
// }
