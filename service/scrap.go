package service

import (
	"context"
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/anaskhan96/soup"
	"github.com/google/uuid"
	pb "gitlab.com/scraping/genproto/product_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Scrap ...
func (p *ProductService) Scrap(ctx context.Context, req *pb.ScrapRequest) (*pb.Empty, error) {

	if req.WebsiteName == "" {
		return nil, status.Error(codes.InvalidArgument, "Invalid website name")
	}

	switch req.WebsiteName {
	case "beautylish":
		{
			products := scrapBeautylish()
			err := p.storage.Product().CreateMultipleProducts(products)
			if err != nil {
				return nil, handleErrorWithMessage(p.logger, err, "error while creating multiple product", req)
			}
		}
	case "dermstore":
		{
			products := scrapDermstore()
			err := p.storage.Product().CreateMultipleProducts(products)
			if err != nil {
				return nil, handleErrorWithMessage(p.logger, err, "error while creating multiple product", req)
			}
		}
	case "ulta":
		{
			products := scrapUlta()
			err := p.storage.Product().CreateMultipleProducts(products)
			if err != nil {
				return nil, handleErrorWithMessage(p.logger, err, "error while creating multiple product", req)
			}
		}
	default:
		return nil, status.Error(codes.InvalidArgument, "Invalid website name, we are not scraping from the websitename you entered")
	}

	return &pb.Empty{}, nil
}

type safeProducts struct {
	products []*pb.Product
	mu       sync.Mutex
}

func scrapBeautylish() []*pb.Product {

	products := make([]*pb.Product, 0, 200)
	productsSafe := safeProducts{
		products: products,
		mu:       sync.Mutex{},
	}

	resp, err := soup.Get("https://www.beautylish.com/")
	if err != nil {
		os.Exit(1)
	}

	doc := soup.HTMLParse(resp)
	letters := doc.FindAll("ul", "class", "catDrop_sublist") //Find("div", "id", "primarymenu_submenu_brand")

	wg := sync.WaitGroup{}
	for _, letter := range letters {
		wg.Add(1)
		go func(letter soup.Root) {

			brands := letter.FindAll("li", "class", "catDrop_item")
			// fmt.Println(letter.Find("a").Text(), "->", brands)
			for _, brand := range brands {
				a := brand.Find("a")
				if a.Pointer == nil {
					continue
				}
				link := "https://www.beautylish.com" + a.Attrs()["href"]

				name := a.Text()
				fmt.Println(name + "-> " + link)

				resp2, err := soup.Get(link)
				if err != nil {
					os.Exit(1)
				}
				doc2 := soup.HTMLParse(resp2)

				ul := doc2.Find("ul", "id", "tiles")

				if ul.Pointer == nil {
					continue
				}
				lis := ul.FindAll("li")

				for _, li := range lis {
					a := li.Find("a")

					if a.Pointer == nil {
						continue
					}

					name := strings.Trim(a.Find("p", "class", "shopTile_itemName").Text(), "\n, ")
					link := a.Attrs()["href"]
					imageURL := a.Find("img").Attrs()["src"]
					brand := strings.Trim(a.Find("p", "class", "shopTile_brand").Text(), "\n, ")
					// fmt.Println("Link:", link)
					// fmt.Println("Image:", imageUrl)
					// fmt.Println("Brand:", brand)
					// fmt.Println("Name:", name)
					// fmt.Println("Price:", strings.Trim(a.Find("p", "class", "shopTile_price").Text(), "\n, "))
					// fmt.Println("")

					id := uuid.New()
					media := pb.MediaForProduct{Value: imageURL, ContentType: "url/"}
					prod := pb.Product{
						Id:            id.String(),
						ProductName:   name,
						Description:   brand,
						Link:          link,
						Rate:          float32(0),
						NumberOfRates: int64(0),
						Medias:        []*pb.MediaForProduct{&media},
						CreatorId:     "",
						ScrapedFrom:   "beautylish.com",
					}

					productsSafe.mu.Lock()
					productsSafe.products = append(productsSafe.products, &prod)
					productsSafe.mu.Unlock()
				}
				break
			}

			wg.Done()
		}(letter)
		break
	}

	wg.Wait()

	return productsSafe.products
}

func scrapBrands(brands []soup.Root) []*pb.Product {

	products := make([]*pb.Product, 0, 200)

	for _, brand := range brands {

		brand := brand.FindAll("li")

		for _, brandName := range brand {
			a := brandName.Find("a")
			if a.Pointer == nil {
				continue
			}
			link := a.Attrs()["href"]
			link = "https://" + link[2:]
			// fmt.Println(link)
			// fmt.Println(strings.Trim(a.Text(), "\n,\t"))

			resp, err := soup.Get(link)
			if err != nil {
				os.Exit(1)
			}

			doc := soup.HTMLParse(resp)
			prds := doc.FindAll("div", "class", "productQvContainer")
			for _, pr := range prds {

				title := pr.Find("div", "class", "prod-title-desc").Find("h4", "class", "prod-title").Find("a").Text()
				title = strings.Trim(title, "\n,\t")

				desc := pr.Find("div", "class", "prod-title-desc").Find("p", "class", "prod-desc").Find("a").Text()
				desc = strings.Trim(desc, "\n,\t")

				// priceDiv := pr.Find("div", "class", "productPrice")
				// price := ""
				// if priceDiv.Pointer != nil {
				// 	price = priceDiv.Find("span").Text()
				// 	price = strings.Trim(price, "\n,\t, ")
				// 	// price = strings.Trim(price, "\t")
				// 	// price = strings.Trim(price, " ")
				// }
				link := pr.Find("div", "class", "prod-title-desc").Find("h4", "class", "prod-title").Find("a").Attrs()["href"]
				link = "https://www.ulta.com" + strings.Trim(link, "\n")

				// fmt.Println(pr.Find("div", "class", "quick-view-prod").Find("a").Find("img").HTML())
				imgLink := pr.Find("div", "class", "quick-view-prod").Find("a").Find("img").Attrs()["data-src"]

				// fmt.Println("title: ", title)
				// fmt.Println("desc: ", desc)
				// fmt.Println("price: ", price)
				// fmt.Println("Link: ", link)
				// fmt.Println("ImageLink: ", imgLink)
				// fmt.Println("")

				id := uuid.New()
				media := pb.MediaForProduct{Value: imgLink, ContentType: "url/"}
				prod := pb.Product{
					Id:            id.String(),
					ProductName:   title,
					Description:   desc,
					Link:          link,
					Rate:          float32(0),
					NumberOfRates: int64(0),
					Medias:        []*pb.MediaForProduct{&media},
					CreatorId:     "",
					ScrapedFrom:   "ulta.com",
				}

				products = append(products, &prod)
			}
		}
	}

	return products
}

func scrapUlta() []*pb.Product {

	products := make([]*pb.Product, 0, 200)
	productsSafe := safeProducts{
		products: products,
		mu:       sync.Mutex{},
	}

	resp, err := soup.Get("https://www.ulta.com/global/nav/allbrands.jsp")
	if err != nil {
		os.Exit(1)
	}

	doc := soup.HTMLParse(resp)
	brandsLists := doc.FindAll("ul", "class", "all-brands-sublisting") //Find("div", "id", "primarymenu_submenu_brand")

	wgExternal := sync.WaitGroup{}
	for _, brandsList := range brandsLists {

		wgExternal.Add(1)
		go func(brandsList soup.Root) {

			brands1 := brandsList.FindAll("ul", "class", "first_column")
			brands2 := brandsList.FindAll("ul", "class", "second_column")
			brands3 := brandsList.FindAll("ul", "class", "third_column")
			brands4 := brandsList.FindAll("ul", "class", "fourth_column")

			// fmt.Println(brands1)
			// fmt.Println(brands2)
			// fmt.Println(brands3)
			// fmt.Println(brands4)
			brands1 = append(brands1, brands2...)
			brands1 = append(brands1, brands3...)
			brands1 = append(brands1, brands4...)

			products := scrapBrands(brands1)

			productsSafe.mu.Lock()
			productsSafe.products = append(productsSafe.products, products...)
			productsSafe.mu.Unlock()

			wgExternal.Done()
		}(brandsList)

		break
	}

	wgExternal.Wait()

	return productsSafe.products
}

func scrapDermstore() []*pb.Product {

	products := make([]*pb.Product, 0, 200)
	productsSafe := safeProducts{
		products: products,
		mu:       sync.Mutex{},
	}

	resp, err := soup.Get("https://www.dermstore.com/brands.list")
	if err != nil {
		os.Exit(1)
	}

	doc := soup.HTMLParse(resp)

	brands := doc.FindAll("li", "class", "responsiveBrandsPageScroll_brandTabsItem")
	// fmt.Println(doc.HTML())
	wgExternal := sync.WaitGroup{}

	for _, brand := range brands {
		a := brand.Find("a")
		if a.Pointer == nil {
			continue
		}

		wgExternal.Add(1)
		go func(brand soup.Root) {

			a := brand.Find("a")
			link := a.Attrs()["href"]
			brandName := a.Text()
			fmt.Println(brandName + "-> " + link)

			resp, err := soup.Get(link)
			if err != nil {
				os.Exit(1)
			}
			doc2 := soup.HTMLParse(resp)

			products := doc2.FindAll("div", "class", "productBlock")
			for _, pr := range products {
				img := pr.Find("img")
				imgURL := img.Attrs()["src"]
				a := pr.Find("div").Find("a")
				link := "https://www.dermstore.com" + a.Attrs()["href"]
				div := pr.Find("div", "class", "productBlock_title")
				name := strings.Trim(div.Find("h3").Text(), "\n, ")

				// fmt.Println("Brand: ", brandName)
				// fmt.Println("link:", link)
				// fmt.Println("name", name)
				// fmt.Println("img:", imgURL)
				// fmt.Println("")

				id := uuid.New()
				media := pb.MediaForProduct{Value: imgURL, ContentType: "url/"}
				prod := pb.Product{
					Id:            id.String(),
					ProductName:   name,
					Description:   brandName,
					Link:          link,
					Rate:          float32(0),
					NumberOfRates: int64(0),
					Medias:        []*pb.MediaForProduct{&media},
					CreatorId:     "",
					ScrapedFrom:   "dermstore.com",
				}

				productsSafe.mu.Lock()
				productsSafe.products = append(productsSafe.products, &prod)
				productsSafe.mu.Unlock()

			}

			wgExternal.Done()
		}(brand)
		break
	}

	wgExternal.Wait()

	return productsSafe.products
}
