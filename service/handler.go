package service

import (
	"database/sql"
	"strings"

	l "gitlab.com/scraping/pkg/logger"
	"gitlab.com/scraping/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	//ErrorCodeInvalidParams ...
	ErrorCodeInvalidParams = "INVALID_PARAMS"
	//ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	//ErrorCodeUnauthorized ...
	ErrorCodeUnauthorized = "UNAUTHORIZED"
	//ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
	//ErrorCodeNotFound ...
	ErrorCodeNotFound = "NOT_FOUND"
	//ErrorBadRequest ...
	ErrorBadRequest = "BAD_REQUEST"
	//ErrorCodeForbidden ...
	ErrorCodeForbidden = "FORBIDDEN"
	//ErrorCodeWrongClub ...
	ErrorCodeWrongClub = "WRONG_CLUB"
	// ErrorInvalidCredentials ...
	ErrorInvalidCredentials = "INVALID_CREDENTIALS"
)

func handleErrorWithMessage(logger l.Logger, err error, message string, req ...interface{}) error {

	if err == repo.ErrAlreadyExists {

		logger.Error(message+", Already Exists", l.Any("req", req), l.Error(err))
		return status.Error(codes.AlreadyExists, "Already Exists")

	} else if err == repo.ErrInvalidField {

		logger.Error(message+" Invalid input", l.Any("req", req), l.Error(err))
		return status.Error(codes.InvalidArgument, "Invalid input")

	} else if err == sql.ErrNoRows {

		logger.Error(message+" Not Found", l.Any("req", req), l.Error(err))
		return status.Error(codes.NotFound, "Not found")

	} else if strings.Contains(err.Error(), "violates foreign key constraint") {

		logger.Error(message+" Invalid input", l.Any("req", req), l.Error(err))
		return status.Error(codes.InvalidArgument, "Invalid input")

	} else if strings.Contains(err.Error(), "violates check constraint") {

		logger.Error(message+" Invalid input", l.Any("req", req), l.Error(err))
		return status.Error(codes.InvalidArgument, "Invalid input")

	} else if strings.Contains(err.Error(), "violates unique constraint") {

		logger.Error(message+" Invalid input", l.Any("req", req), l.Error(err))
		return status.Error(codes.AlreadyExists, "Invalid input")

	} else if strings.Contains(err.Error(), "forbidden") {

		logger.Error(message+" Invalid input forbidden", l.Any("req", req), l.Error(err))
		return status.Error(codes.PermissionDenied, "Not allowed")

	} else if strings.Contains(err.Error(), "invalid input syntax for type uuid") {

		logger.Error(message+" Invalid input", l.Any("req", req), l.Error(err))
		return status.Error(codes.InvalidArgument, "Invalid id for type uuid")

	} else if err != nil {

		logger.Error(message+" unknown error", l.Error(err), l.Any("req", req))
		return status.Error(codes.Internal, "Internal server error")

	}

	return nil
}
