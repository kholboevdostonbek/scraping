package service

import (
	"gitlab.com/scraping/config"
	l "gitlab.com/scraping/pkg/logger"
	"gitlab.com/scraping/storage"
)

// ProductService ...
type ProductService struct {
	storage storage.I
	logger  l.Logger
	config  *config.Config
}

// NewProductService ...
func NewProductService(log l.Logger) *ProductService {
	return &ProductService{
		storage: storage.NewStorage(),
		logger:  log,
		config:  &config.Config{},
	}
}
