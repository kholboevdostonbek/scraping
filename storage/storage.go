package storage

import (
	"gitlab.com/scraping/storage/postgres"
	"gitlab.com/scraping/storage/repo"
)

// I is an interface for storage
type I interface {
	Product() repo.ProductRepository
}

type storage struct {
	productRepo repo.ProductRepository
}

// NewStorage ...
func NewStorage() I {
	return &storage{
		productRepo: postgres.NewProductRepo(),
	}
}

func (s storage) Product() repo.ProductRepository {
	return s.productRepo
}
