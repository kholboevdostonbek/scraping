package postgres

import (
	"database/sql/driver"
	"fmt"
)

// Useful link for parsing composite type array
// https://stackoverflow.com/questions/59353838/go-insert-composite-type-array-unsupported-type

// Media ...
type Media struct {
	ID          string
	ContentType string
}

//MediaSlice ...
type MediaSlice []*Media

//Scan ...
func (s *MediaSlice) Scan(src interface{}) error {
	var a []byte // the pq array as bytes
	switch v := src.(type) {
	case []byte:
		a = v
	case string:
		a = []byte(v)
	case nil:
		*s = nil
		return nil
	default:
		return fmt.Errorf("urlslice.Scan unexpected src type %T", src)
	}

	a = a[1 : len(a)-1] // drop curly braces
	for i := 0; i < len(a); i++ {
		if a[i] == '"' && (len(a) > (i+1) && a[i+1] == '(') { // element start?
			i += 2 // move past `"(`
			j := i // start of media.id
			u := Media{}

			for ; i < len(a) && a[i] != ','; i++ {
			}
			u.ID = string(a[j:i])

			i++   // move past `,`
			j = i // start of media.ContentType
			for ; i < len(a) && a[i] != ')'; i++ {
			}
			u.ContentType = string(a[j:i])

			*s = append(*s, &u)

			i += 2 // move past `)",`
		}
	}
	return nil
}

//Value ....
func (s MediaSlice) Value() (driver.Value, error) {
	data := []byte{'{'}
	for _, media := range s {
		data = append(data, '"', '(')
		data = append(data, []byte(media.ID)...)
		data = append(data, ',')
		data = append(data, []byte(media.ContentType)...)
		data = append(data, ')', '"', ',')
	}
	data[len(data)-1] = '}' // replace last ',' with '}' to close the array
	return data, nil
}

// MediaForProduct ...
type MediaForProduct struct {
	Value       string
	ContentType string
}

//MediaSliceForProduct ...
type MediaSliceForProduct []*MediaForProduct

//Scan ...
func (s *MediaSliceForProduct) Scan(src interface{}) error {
	var a []byte // the pq array as bytes
	switch v := src.(type) {
	case []byte:
		a = v
	case string:
		a = []byte(v)
	case nil:
		*s = nil
		return nil
	default:
		return fmt.Errorf("urlslice.Scan unexpected src type %T", src)
	}

	a = a[1 : len(a)-1] // drop curly braces
	for i := 0; i < len(a); i++ {
		if a[i] == '"' && (len(a) > (i+1) && a[i+1] == '(') { // element start?
			i += 2 // move past `"(`
			u := MediaForProduct{}

			j := i // start of media.ContentType
			for ; i < len(a) && a[i] != ','; i++ {
			}
			u.ContentType = string(a[j:i])
			
			i++   // move past `,`
			
			j = i // start of media.id
			for ; i < len(a) && a[i] != ')'; i++ {
			}
			u.Value = string(a[j:i])

			*s = append(*s, &u)

			i += 2 // move past `)",`
		}
	}
	return nil
}

//Value ....
func (s MediaSliceForProduct) Value() (driver.Value, error) {
	data := []byte{'{'}
	for _, media := range s {
		data = append(data, '"', '(')
		data = append(data, []byte(media.ContentType)...)
		data = append(data, ',')
		data = append(data, []byte(media.Value)...)
		data = append(data, ')', '"', ',')
	}
	data[len(data)-1] = '}' // replace last ',' with '}' to close the array
	return data, nil
}
