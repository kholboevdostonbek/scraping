package postgres

import (
	"github.com/jmoiron/sqlx"
	// "google.golang.org/protobuf/internal/errors"

	"gitlab.com/scraping/platform/postgres"
	"gitlab.com/scraping/storage/repo"
)

type productRepo struct {
	db *sqlx.DB
}

// NewProductRepo ...
func NewProductRepo() repo.ProductRepository {

	return &productRepo{
		db: postgres.DB(),
	}
}
