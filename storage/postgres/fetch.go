package postgres

import (
	"database/sql"
	"log"
	"strings"

	pb "gitlab.com/scraping/genproto/product_service"
	"gitlab.com/scraping/pkg/utils"
)

//CreateMultipleProducts ...
func (pr *productRepo) CreateMultipleProducts(products []*pb.Product) error {

	nullString := sql.NullString{}

	tx, err := pr.db.Begin()
	defer func() {
		err := tx.Commit()
		if err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		log.Println("Error beginning transaction")
		return err
	}
	qry := `INSERT INTO products (
		    id,
			name,
			description,
		    link,
            medias,
			created_at,
			creator_id,
			scraped_from
		)
		VALUES `

	vals := []interface{}{}
	for _, product := range products {

		medias := make(MediaSliceForProduct, 0, len(product.Medias))

		for _, mediaData := range product.Medias {
			medias = append(medias, &MediaForProduct{
				ContentType: "url/",
				Value:       mediaData.Value},
			)
		}
		qry += `(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?),`
		vals = append(vals,
			product.Id,
			product.ProductName,
			product.Description,
			product.Link,
			medias,
			nullString,
			product.ScrapedFrom)

	}

	// trim the last ,
	qry = strings.TrimSuffix(qry, `,`)
	// Replacing ? with $n for postgres
	qry = utils.ReplaceSQL(qry, `?`)
	qry += " ON CONFLICT ON CONSTRAINT unique_product_link_cons DO NOTHING "
	stmt, err := tx.Prepare(qry)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	_, err = stmt.Exec(vals...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}
