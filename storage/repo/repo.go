package repo

import (
	"errors"

	pb "gitlab.com/scraping/genproto/product_service"
)

var (
	// ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("already exists")
	// ErrInvalidField ...
	ErrInvalidField = errors.New("incorrect field")
)

// ProductRepository is an interface for client storage
type ProductRepository interface {
	Reader
	Writer
}

// Reader - this interface is used for selecting a data
type Reader interface {
}

// Writer - this interface is used for inserting a data
type Writer interface {
	// Product
	CreateMultipleProducts([]*pb.Product) error
}
